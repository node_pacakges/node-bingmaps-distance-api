"use strict";

const qs = require("querystring");
const axios = require("axios").default;

var DISTANCE_API_URL = "https://dev.virtualearth.net/REST/V1/Routes/";

var BingDistance = function () {
  this.apiKey = "";
  this.businessClientKey = "";
  this.businessSignatureKey = "";
};

BingDistance.prototype.get = function (args) {
  var options = formatOptions.call(this, args);
  return new Promise((resolve, reject) => {
    fetchData(options, function (err, data) {
      if (err) return err;
      formatResults(data, options, function (err, results) {
        if (err) reject(err);
        else resolve(results);
      });
    });
  });
};

var formatOptions = function (args) {
  args.units = "km";
  var options = {
    origin: args.origin,
    destination: args.destination,
    travelMode: args.mode || "driving",
    distanceUnit: args.units || "km",
    language: args.language || "en",
    avoid: args.avoid || null,
    optimize: args.optimize || "time",
    key: this.apiKey,
  };

  if (this.businessClientKey && this.businessSignatureKey) {
    delete options.key;
    options.client = this.businessClientKey;
    options.signature = this.businessSignatureKey;
  }
  if (!options.origin) {
    throw new Error("Argument Error: Origin is invalid");
  }
  if (!options.destination) {
    throw new Error("Argument Error: Destination is invalid");
  }
  return options;
};

var formatResults = function (data, options, callback) {
  var formatData = function (element) {
    return {
      distance: element.distance.text,
      distanceValue: element.distance.value,
      duration: element.duration.text,
      durationValue: element.duration.value,
      mode: options.travelMode,
      units: options.distanceUnit,
      language: options.language,
      avoid: options.avoid,
      optimize: options.optimize,
    };
  };

  var requestStatus = data.status;
  if (requestStatus != "OK") {
    return callback(
      new Error("Status error: " + requestStatus + ": " + data.error_message)
    );
  }
  var results = [];
  var element = data;
  var resultStatus = element.status;
  if (resultStatus != "OK") {
    return callback(new Error("Result error: " + resultStatus));
  }

  results.push(formatData(element));

  if (results.length == 1) {
    results = results[0];
  }
  return callback(null, results);
};

var fetchData = function (options, callback) {
  let mode;
  switch (options.travelMode) {
    case "driving":
      mode = "Driving";
      break;
    case "walking":
      mode = "Walking";
      break;

    default:
      mode = "Driving";
      break;
  }

  options["wp.0"] = options.origin;
  options["wp.1"] = options.destination;

  delete options.origin;
  delete options.destination;

  function duas_casas(numero) {
    if (numero <= 9) {
      numero = "0" + numero;
    }
    return numero;
  }

  axios
    .get(`${DISTANCE_API_URL}${mode}?${qs.stringify(options)}`)
    .then(function (response) {
      var data = response.data;
      let info = {
        status: "OK",
        distance: {
          value: null,
          text: null,
        },
        duration: {
          value: null,
          text: null,
        },
      };

      info.distance.value = parseFloat(
        data.resourceSets[0].resources[0].travelDistance.toFixed(2)
      );
      info.distance.text = `${info.distance.value} km`;

      // DURATION //
      info.duration.value = data.resourceSets[0].resources[0].travelDuration;

      let hora = duas_casas(Math.round(info.duration.value / 3600));
      let minuto = duas_casas(Math.round((info.duration.value % 3600) / 60));
      let segundo = duas_casas((info.duration.value % 3600) % 60);
      let formatado = hora + ":" + minuto + ":" + segundo;
      info.duration.text = formatado;

      callback(null, info);
    })
    .catch(function (error) {
      let err = error;
      err.status = "error";
      err.error_message = err;
      if (error.response) err = error.response.data;

      callback(
        new Error(
          "Request error: Could not fetch data from Google's servers: " + err
        )
      );
    });
};

module.exports = new BingDistance();
