# Bing Distance API for Node.js

Easily get traveling distance and duration data between locations with the [Bingmaps Distance API](https://docs.microsoft.com/en-us/bingmaps/rest-services/routes/calculate-a-route?redirectedfrom=MSDN)

## Installation

    npm install hpsweb-bingmaps-distance

## Usage

```js
var distance = require("hpsweb-bingmaps-distance");

distance
  .get({
    origin: "San Francisco, CA",
    destination: "San Diego, CA",
  })
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  });
```

The above example outputs the following `data` object:

```js
{
  index: null,
  distance: '807 km',
  distanceValue: 807366,
  duration: '7 hours 30 mins',
  durationValue: 26981,
  origin: 'San Francisco, CA, USA',
  destination: 'San Diego, CA, USA',
  mode: 'driving',
  units: 'metric',
  language: 'en',
  avoid: null,
  sensor: false
}
```

#### Batch Mode

This example will return an array of `data` objects corresponding to all origin/destination pairs.

```js
distance
  .get({
    origins: ["San Francisco, CA", "San Diego, CA"],
    destinations: ["San Diego, CA", "Seattle, WA"],
  })
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  });
```

Result:

| Origin            | Destination   |
| ----------------- | ------------- |
| San Francisco, CA | San Diego, CA |
| San Francisco, CA | Seattle, WA   |
| San Diego, CA     | San Diego, CA |
| San Diego, CA     | Seattle, WA   |

## Additional Parameters

Here is a full list of options you can include to tailor your query:

- origin, destination - `name` (eg. `'San Francisco, CA'`) | `latitude/longitude` (eg. `'51.510652,-0.095444'`)
- index - `null` (default) | specify an index for identification
- mode - `'driving'` (default) | `'walking'` | `'bicycling'`
- units - `'km'` (default) Kilometer(km)/Mile(mi) | `'imperial'` miles/feet
- avoid - `null` (default) | `'highways'` | `'tolls'` | `'ferry'` | `'minimizeHighways'` | `'minimizeTolls'` | `'borderCrossing'`
- optimize - `time` (default) | `distance` | `timeWithTraffic` | `timeAvoidClosure` | Specifies what parameters to use to optimize the route.

Note: The `units` setting only affects the text displayed within `distance` fields.

`distanceValue` is always in meters, and `durationValue` is always in seconds.

## More Examples

This example specifies `mode` and `units`:

```js
distance
  .get({
    origin: "San Francisco, CA",
    destination: "Los Angeles, CA",
    mode: "bicycling",
    units: "imperial",
  })
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  });
```

Outputs:

```js
{
  index: null,
  distance: '499 mi',
  distanceValue: 802534,
  duration: '1 day 21 hours',
  durationValue: 161896,
  origin: 'San Francisco, CA, USA',
  destination: 'Los Angeles, CA, USA',
  mode: 'bicycling',
  units: 'imperial',
  language: 'en',
  avoid: null,
  sensor: false
}
```

---

Let's use latitude and longitude for our origin/destination and an index:

```js
distance
  .get({
    index: 1,
    origin: "37.772886,-122.423771",
    destination: "37.871601,-122.269104",
  })
  .then(function (data) {
    console.log(data);
  })
  .catch(function (err) {
    console.log(err);
  });
```

Outputs:

```js
{
  index: 1,
  distance: '21.9 km',
  distanceValue: 21946,
  duration: '21 mins',
  durationValue: 1251,
  origin: 'Octavia Boulevard, San Francisco, CA 94102, USA',
  destination: '2066-2070 University Avenue, Berkeley, CA 94704, USA',
  mode: 'driving',
  units: 'metric',
  language: 'en',
  avoid: null,
  sensor: false
}
```

## API Keys

You can request a key by [following these steps](https://www.bingmapsportal.com/Application).

Specify an API key for use like this:

```js
var distance = require("hpsweb-bingmaps-distance");
distance.apiKey = "API_KEY";
```

Business users can omit the API key and instead specify their business client and signature keys:

```js
var distance = require("hpsweb-bingmaps-distance");
distance.businessClientKey = "CLIENT_KEY";
distance.businessSignatureKey = "SIGNATURE_KEY";
```

## Running Tests

1. Install the development dependencies:

   npm install

2. Run the tests:

   npm test
